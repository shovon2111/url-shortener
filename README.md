The apis of this project are documented using swagger.
You can find api documentation in this url

http://localhost:8080/swagger-ui.html#/


To visit the long url though short url the link should be
in below format - 

http://localhost:8080/api/<short-url>

Example: http://localhost:8080/api/bsfd9f

