package com.dev.service;

import com.dev.domain.Url;
import com.dev.utils.JsonMapper;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class ParamExtractorService {

    public String concatParam(Url url) throws ArrayIndexOutOfBoundsException, RuntimeException {

        if (url.getLongUrl().contains("?")) {
            return generateLongUrlWithPredefinedParamKey(url);
        } else {
            return generateLongUrlWithOutPredefinedParamKey(url);
        }
    }


    /*
        "long_url": "http://example.com/path1/path2/path3?arg=value&arg1=value",
        "parameters": {
            "param1": "value1",
            "param2": "value2"
        }
    */
    private String generateLongUrlWithPredefinedParamKey(Url url) throws ArrayIndexOutOfBoundsException, RuntimeException {
        List<String> paramList = generateParamList(url);
        if (paramList.isEmpty())
            return url.getLongUrl();

        int paramCount = 0;
        boolean flag = true;
        int index = url.getLongUrl().indexOf('?');
        url.setLongUrl(url.getLongUrl() + "&");
        StringBuffer buffer = new StringBuffer(url.getLongUrl().substring(0, index));
        int len = url.getLongUrl().length();
        while (index < len) {
            if (flag) {
                buffer.append(url.getLongUrl().charAt(index));
            }
            if (url.getLongUrl().charAt(index) == '=') {
                buffer.append(paramList.get(paramCount));
                paramCount++;
                flag = false;
            }
            if (url.getLongUrl().charAt(index) == '&') {
                buffer.append(url.getLongUrl().charAt(index));
                flag = true;
            }
            index++;
        }
        return buffer.toString().substring(0, buffer.length() - 1);
    }

    /*
        "long_url": "http://example.com/path1/path2/path3",
        "parameters": {
            "param1": "value1",
            "param2": "value2"
        }
    */
    private String generateLongUrlWithOutPredefinedParamKey(Url url) throws ArrayIndexOutOfBoundsException, RuntimeException {
        StringBuffer buffer = new StringBuffer(url.getLongUrl() + "?");
        extractParamMap(url).entrySet()
                .forEach(entry -> buffer.append(entry.getKey() + "=" + entry.getValue() + "&"));
        return buffer.toString().substring(0, buffer.length() - 1);
    }


    private List<String> generateParamList(Url url) {
        return extractParamMap(url)
                .entrySet().stream()
                .map(entry -> entry.getValue())
                .collect(Collectors.toList());
    }

    private Map<String, String> extractParamMap(Url url) throws RuntimeException {
        Map params = (Map) JsonMapper.mapToObject(url.getParameters());
        if (params.isEmpty()) {
            return Collections.EMPTY_MAP;
        }
        return params;
    }
}
