package com.dev.service;

import com.dev.domain.Url;
import com.dev.dto.responsedto.UrlResponseDTO;

public interface UrlService {

    UrlResponseDTO getUrlDetails(int id);

    Url getUrlByShortUrl(String shortUrl);

    long countTotalShortUrls();
}
