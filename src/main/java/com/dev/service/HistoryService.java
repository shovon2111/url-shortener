package com.dev.service;

import com.dev.dto.projection.VisitedUrlProjection;
import org.springframework.mobile.device.Device;
import org.springframework.scheduling.annotation.Async;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public interface HistoryService {

    Integer saveClickHistory(int urlId,
                         HttpServletRequest request,
                         Device device);

    List<VisitedUrlProjection> getAllVisitedUrls();
}
