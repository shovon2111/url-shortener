package com.dev.service.impl;

import com.dev.domain.Url;
import com.dev.dto.responsedto.UrlResponseDTO;
import com.dev.exceptions.ResourceNotFoundExceptionHandler;
import com.dev.repo.UrlRepo;
import com.dev.service.ParamExtractorService;
import com.dev.service.UrlService;
import com.dev.utils.JsonMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
public class UrlServiceImpl implements UrlService {

    private final UrlRepo urlRepo;
    private final ParamExtractorService paramExtractorService;

    public UrlServiceImpl(UrlRepo urlRepo,
                          ParamExtractorService paramExtractorService) {
        this.urlRepo = urlRepo;
        this.paramExtractorService = paramExtractorService;
    }

    @Override
    public UrlResponseDTO getUrlDetails(int id) {
        Url url = urlRepo.findByIdAndDomainStatus(id, true)
                .orElseThrow(() -> new ResourceNotFoundExceptionHandler(
                        "Url not found by Id " + id,
                        "Url not found by Id " + id));
        return convertUrlToResponseDTO(url);
    }

    @Override
    public Url getUrlByShortUrl(String shortUrl) {
        Url url = urlRepo.findByShortUrlAndDomainStatus(shortUrl, true)
                .orElseThrow(() -> new ResourceNotFoundExceptionHandler(
                        "Url not found by",
                        "Url not found by"));
        if (url.getParameters() != null && !url.getParameters().isEmpty()) {
            String concatenatedUrl = paramExtractorService.concatParam(url);
            url.setLongUrl(concatenatedUrl);
        }
        return url;
    }

    @Override
    public long countTotalShortUrls() {
        return urlRepo.countByDomainStatus(true);
    }

    public UrlResponseDTO convertUrlToResponseDTO(Url url) {
        UrlResponseDTO urlShortenerRequestDTO = new UrlResponseDTO();
        BeanUtils.copyProperties(url, urlShortenerRequestDTO);
        urlShortenerRequestDTO.setParameters((Map<String, String>) JsonMapper.mapToObject(url.getParameters()));
        return urlShortenerRequestDTO;
    }


}
