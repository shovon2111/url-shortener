package com.dev.service.impl;

import com.dev.domain.History;
import com.dev.dto.projection.VisitedUrlProjection;
import com.dev.repo.HistoryRepo;
import com.dev.service.HistoryService;
import com.dev.service.UserInformationService;
import org.springframework.mobile.device.Device;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Service
public class HistoryServiceImpl implements HistoryService {

    private final UserInformationService userInformationService;
    private final HistoryRepo historyRepo;

    public HistoryServiceImpl(UserInformationService userInformationService,
                              HistoryRepo historyRepo) {
        this.userInformationService = userInformationService;
        this.historyRepo = historyRepo;
    }

    @Override
    @Transactional
    public Integer saveClickHistory(int urlId,
                                    HttpServletRequest request,
                                    Device device) {
        History history = constructHistoryEntity(urlId, request, device);
        return this.historyRepo.save(history).getId();
    }

    @Override
    public List<VisitedUrlProjection> getAllVisitedUrls() {
        return historyRepo.getAllVisitedUrls();
    }

    History constructHistoryEntity(int urlId, HttpServletRequest request, Device device) {
        History history = new History();
        history.setUrlId(urlId);
        history.setIp(userInformationService.getClientIp(request));
        history.setBrowser(userInformationService.getClientBrowser(request));
        history.setDevice(userInformationService.getClientDevice(device));
        return history;
    }
}
