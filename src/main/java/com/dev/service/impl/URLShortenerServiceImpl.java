package com.dev.service.impl;

import com.dev.domain.Url;
import com.dev.dto.requestDto.URLShortenerRequestDTO;
import com.dev.repo.UrlRepo;
import com.dev.service.Shortener;
import com.dev.service.URLShortenerService;
import com.dev.utils.JsonMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class URLShortenerServiceImpl implements URLShortenerService {

    private final UrlRepo urlRepo;
    private final Shortener shortener;

    public URLShortenerServiceImpl(UrlRepo urlRepo,
                                   Shortener shortener) {
        this.urlRepo = urlRepo;
        this.shortener = shortener;
    }

    @Override
    public int saveAndGenerateShortUrl(URLShortenerRequestDTO urlShortenerRequestDTO) {

        int id = getMaxId() + 1;
        String shortUrl = this.shortener.shortUrl(id);
        return this.saveUrl(urlShortenerRequestDTO, shortUrl);
    }

    public Url urlDtoToEntity(URLShortenerRequestDTO urlShortenerRequestDTO) {
        Url url = new Url();
        BeanUtils.copyProperties(urlShortenerRequestDTO, url);
        return url;
    }

    private int getMaxId() {
        return urlRepo.getMaxId()
                .orElseGet(() -> 0);
    }

    @Transactional
    private int saveUrl(URLShortenerRequestDTO urlShortenerRequestDTO, String shortUrl) {
        Url url = this.urlDtoToEntity(urlShortenerRequestDTO);
        url.setShortUrl(shortUrl);
        if (urlShortenerRequestDTO.getParameters() != null) {
            url.setParameters(JsonMapper.mapToJson(urlShortenerRequestDTO.getParameters()));
        }
        return urlRepo.save(url).getId();
    }
}
