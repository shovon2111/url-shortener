package com.dev.service;

import org.springframework.mobile.device.Device;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;

@Async
@Service
public class AsyncHistoryService {

    private final HistoryService historyService;

    public AsyncHistoryService(HistoryService historyService) {
        this.historyService = historyService;
    }

    public Integer saveClickHistory(int urlId,
                                HttpServletRequest request,
                                Device device) {
        return this.historyService.saveClickHistory(urlId, request, device);
    }
}
