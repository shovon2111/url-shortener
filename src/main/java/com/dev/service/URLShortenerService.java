package com.dev.service;

import com.dev.dto.requestDto.URLShortenerRequestDTO;

public interface URLShortenerService {

    int saveAndGenerateShortUrl(URLShortenerRequestDTO urlShortenerRequestDTO);
}
