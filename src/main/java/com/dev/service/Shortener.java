package com.dev.service;

import com.dev.exceptions.GenericException;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class Shortener {

    static char map[] = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789".toCharArray();

    public String shortUrl(int n) throws RuntimeException {
        return this.generateShortUrl(this.convertIdUrl(n));
    }

    private String convertIdUrl(int n) {
        StringBuffer shorturl = new StringBuffer();
        try {
            while (n > 0) {
                shorturl.append(map[n % 62]);
                n = n / 62;
            }
        } catch (ArrayIndexOutOfBoundsException ex) {
            String message = this.getClass().getSimpleName() + "-" + this.getClass().getEnclosingMethod().getName() +
                    "ArrayIndexOutOfBoundsException occurred: " + ex.getMessage();
            throw new GenericException(message, message);
        }
        return shorturl.reverse().toString();
    }

    private String generateShortUrl(String shortUrl) throws RuntimeException {
        if (!shortUrl.isEmpty() && shortUrl.length() < 6) {
            String uuid = UUID.randomUUID().toString().substring(0, (6 - shortUrl.length()));
            shortUrl = shortUrl + uuid;
        }
        return shortUrl;
    }
}
