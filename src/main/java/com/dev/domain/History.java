package com.dev.domain;

import com.dev.utils.custom.annotations.NotEmptyOrNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "history")
public class History extends BaseEntity {

    @Column(name = "url_id")
    @NotNull(message = "Short-Url can't be null or empty")
    private Integer urlId;

    @Column(name = "ip")
    @NotEmptyOrNull(message = "IP can't be null or empty")
    private String ip;

    @Column(name = "browser")
    @NotEmptyOrNull(message = "Browser can't be null or empty")
    private String browser;

    @Column(name = "device")
    @NotEmptyOrNull(message = "Device can't be null or empty")
    private String device;
}
