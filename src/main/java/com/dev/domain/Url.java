package com.dev.domain;

import com.dev.utils.custom.annotations.NotEmptyOrNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;


@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "urls")
public class Url extends BaseEntity {

    @Column(name = "short_url")
    @NotEmptyOrNull(message = "Short-Url can't be null or empty")
    private String shortUrl;

    @Column(name = "short_url_domain")
    @NotEmptyOrNull(message = "Short-Url-Domain can't be null or empty")
    private String shortUrlDomain;

    @Column(name = "long_url")
    @NotEmptyOrNull(message = "Long-Url can't be null or empty")
    private String longUrl;

    @Column(name = "params")
    private String parameters;
}
