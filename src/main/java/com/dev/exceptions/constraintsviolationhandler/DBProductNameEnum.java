package com.dev.exceptions.constraintsviolationhandler;

public enum DBProductNameEnum {
    MySQL("MySQL");

    private String value;

    DBProductNameEnum(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
