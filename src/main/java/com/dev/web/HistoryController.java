package com.dev.web;

import com.dev.dto.responsedto.SuccessResponse;
import com.dev.service.HistoryService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class HistoryController {

    private final HistoryService historyService;

    public HistoryController(HistoryService historyService) {
        this.historyService = historyService;
    }

    @GetMapping("/visit/history")
    public ResponseEntity<?> getAllVisitedUrls() {
        return ResponseEntity.ok().body(new SuccessResponse(historyService.getAllVisitedUrls()));
    }
}
