package com.dev.web;

import com.dev.dto.requestDto.URLShortenerRequestDTO;
import com.dev.dto.responsedto.SuccessResponse;
import com.dev.service.URLShortenerService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping("/api")
public class URLShortenerController {

    private final URLShortenerService urlShortenerService;

    public URLShortenerController(URLShortenerService urlShortenerService) {
        this.urlShortenerService = urlShortenerService;
    }

    @PostMapping("/generate/short-url")
    public ResponseEntity<?> save(@Valid @RequestBody URLShortenerRequestDTO urlShortenerRequestDTO) throws RuntimeException{
        return new ResponseEntity(new SuccessResponse(
                urlShortenerService.saveAndGenerateShortUrl(urlShortenerRequestDTO)), HttpStatus.CREATED);
    }
}
