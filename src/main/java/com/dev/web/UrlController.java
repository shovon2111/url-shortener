package com.dev.web;

import com.dev.domain.Url;
import com.dev.dto.responsedto.SuccessResponse;
import com.dev.service.AsyncHistoryService;
import com.dev.service.UrlService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mobile.device.Device;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.net.URI;

@RestController
@RequestMapping("/api")
public class UrlController {

    private final UrlService urlService;
    private final AsyncHistoryService asyncHistoryService;

    public UrlController(UrlService urlService,
                         AsyncHistoryService asyncHistoryService) {
        this.urlService = urlService;
        this.asyncHistoryService = asyncHistoryService;
    }

    @GetMapping("/url/details/id")
    public ResponseEntity<?> getUrlDetailsById(@RequestParam("id") int id) {
        return ResponseEntity.ok().body(new SuccessResponse(urlService.getUrlDetails(id)));
    }

    @GetMapping("/{url}")
    public ResponseEntity<?> getUrlDetailsById(@PathVariable("url") String shortUrl,
                                               HttpServletRequest request,
                                               Device device) {
        Url url = urlService.getUrlByShortUrl(shortUrl);
        asyncHistoryService.saveClickHistory(url.getId(), request, device);
        return ResponseEntity.status(HttpStatus.FOUND)
                .location(URI.create(url.getLongUrl())).build();
    }

    @GetMapping("/url/count")
    public ResponseEntity<?> countTotalShortUrls() {
        return ResponseEntity.ok().body(new SuccessResponse(urlService.countTotalShortUrls()));
    }
}
