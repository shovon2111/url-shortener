package com.dev.repo;

import com.dev.domain.History;
import com.dev.dto.projection.VisitedUrlProjection;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface HistoryRepo extends JpaRepository<History, Integer> {

    @Query(value = "SELECT " +
            "u.short_url AS shortUrl, " +
            "u.short_url_domain AS shortUrlDomain, " +
            "u.long_url AS longUrl, " +
            "h.ip AS ip, " +
            "h.browser AS browser, " +
            "h.device AS device " +
            "FROM urls u " +
            "INNER JOIN history h " +
            "ON u.id = h.url_id " +
            "AND u.domain_status = 1 " +
            "AND h.domain_status = 1", nativeQuery = true)
    List<VisitedUrlProjection> getAllVisitedUrls();
}
