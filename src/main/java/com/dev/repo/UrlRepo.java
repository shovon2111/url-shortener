package com.dev.repo;

import com.dev.domain.Url;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Optional;

public interface UrlRepo extends JpaRepository<Url, Integer> {

    @Query(value = "SELECT u.id FROM urls u ORDER BY u.id DESC LIMIT 1", nativeQuery = true)
    Optional<Integer> getMaxId();

    Optional<Url> findByIdAndDomainStatus(int id, boolean status);

    Optional<Url> findByShortUrlAndDomainStatus(String shortUrl, boolean status);

    long countByDomainStatus(boolean status);
}
