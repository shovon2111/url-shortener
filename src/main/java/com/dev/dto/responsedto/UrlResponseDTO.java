package com.dev.dto.responsedto;

import lombok.Data;

import java.util.Map;

@Data
public class UrlResponseDTO {

    String longUrl;
    String shortUrl;
    String shortUrlDomain;
    Map<String, String> parameters;
}
