package com.dev.dto.requestDto;

import com.dev.utils.custom.annotations.NotEmptyOrNull;
import lombok.Data;

import java.util.Map;

@Data
public class URLShortenerRequestDTO {

    @NotEmptyOrNull(message = "Long-Url can't be empty or null")
    String longUrl;
    @NotEmptyOrNull(message = "Short-Url-Domain can't be empty or null")
    String shortUrlDomain;
    Map<String, String> parameters;

}
