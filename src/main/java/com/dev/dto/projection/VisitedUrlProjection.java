package com.dev.dto.projection;

public interface VisitedUrlProjection {

    public String getShortUrl();

    public String getShortUrlDomain();

    public String getLongUrl();

    public String getIp();

    public String getBrowser();

    public String getDevice();
}
